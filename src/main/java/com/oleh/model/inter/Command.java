package com.oleh.model.inter;

@FunctionalInterface
public interface Command {
    void execute(String s);
}
