package com.oleh.model.comand;

import com.oleh.model.inter.Command;

public class MakeVoice implements Command {

    @Override
    public void execute(String s) {
        System.out.println("Making voice " + s);

    }
}
