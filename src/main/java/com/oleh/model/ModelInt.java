package com.oleh.model;

import com.oleh.model.inter.Command;

import java.util.List;
import java.util.Map;

public interface ModelInt {
    int getMaxOfThreeNumb(int a, int b, int c);

    int getAvgOfThreeNumb(int a, int b, int c);

    Command testCommandPattern(String commandName);

    double getAvg(List<Integer> integerList);

    int getMin(List<Integer> integerList);

    int getMax(List<Integer> integerList);

    int countSumWithReduce(List<Integer> integerList);

    int countSumWithSum(List<Integer> integerList);

    long getAmountOfValBiggerThanAvg(List<Integer> integerList);

    long getNumberOfUniqueWords(List<String> strings);

    List<String> getSortedListOfUniqueWords(List<String> strings);

    Map<String, Long> occurrenceOfEachWord(List<String> strings);

    Map<Character, Long> occurrenceOfEachSymbolNoUpper(List<String> strings);
}
