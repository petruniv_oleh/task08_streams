package com.oleh.model;

import com.oleh.model.comand.MakeVoice;
import com.oleh.model.inter.Command;
import com.oleh.model.inter.Inteble;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Model implements ModelInt {
    private final int MIN_INT = 150;
    private final int MAX_INT = 4000;

    private static Logger logger = LogManager.getLogger(Model.class.getName());


    @Override
    public int getMaxOfThreeNumb(int a, int b, int c) {
        logger.info("Invoke getMaxOfThreeNumb");
        logger.debug("a=" + a + " b=" + b + " c=" + c);
        Inteble max = (x, y, z) -> {
            int maxVal = x;
            if (y > maxVal) {
                maxVal = y;
            }
            if (z > maxVal) {
                maxVal = z;
            }
            return maxVal;
        };
        logger.info("Calculating max");
        int maxVal = max.getIntValue(a, b, c);
        logger.debug("Max is " + maxVal);
        return maxVal;
    }

    @Override
    public int getAvgOfThreeNumb(int a, int b, int c) {
        logger.info("Invoke getAvgOfThreeNumb");
        Inteble avg = (x, y, z) -> (x + y + z) / 3;
        logger.info("Calculating avg");
        int avgVal = avg.getIntValue(a, b, c);
        logger.debug("Avg is " + avgVal);
        return avgVal;
    }


    @Override
    public Command testCommandPattern(String commandName) throws NullPointerException {
        logger.info("Invoke testCommandPattern");
        Map<String, Command> commandMap = new LinkedHashMap<>();
        Command run = (s) -> {
            System.out.println("Running " + s);
        };
        commandMap.put("run", run);

        Command jump = new Command() {
            @Override
            public void execute(String s) {
                System.out.println("Jumping " + s);
            }
        };
        commandMap.put("jump", jump);
        Command dive = Model::commandMethod;
        commandMap.put("dive", dive);
        Command voice = new MakeVoice();
        commandMap.put("voice", voice);

        try {
            return commandMap.get(commandName);
        } catch (NullPointerException e) {
            throw new NullPointerException("No such command");
        }
    }

    private static void commandMethod(String s) {
        System.out.println("Dive " + s);
    }


    public List<Integer> getRandomIntList() {
        return Stream.generate(new Random()::nextInt)
                .filter(i -> i < 1000 && i > -100)
                .limit(20)
                .limit(10).collect(Collectors.toList());
    }

    public List<Integer> getRandomIntListAnotherWay() {
        Random random = new Random();
        IntStream ints = random.ints(MIN_INT, MAX_INT);
        return ints.skip(15)
                .limit(10)
                .boxed()
                .collect(Collectors.toList());
    }


    @Override
    public double getAvg(List<Integer> integerList) {
        return integerList.stream()
                .mapToDouble(i -> i)
                .average().getAsDouble();
    }

    @Override
    public int getMin(List<Integer> integerList) {
        return integerList.stream()
                .min(Integer::compareTo).get();
    }

    @Override
    public int getMax(List<Integer> integerList) {
        return integerList.stream()
                .max(Integer::compareTo).get();
    }

    @Override
    public int countSumWithReduce(List<Integer> integerList) {
        return integerList.stream()
                .reduce(0, (x1, x2) -> x1 + x2);
    }

    @Override
    public int countSumWithSum(List<Integer> integerList) {
        return integerList.stream()
                .mapToInt(i -> i)
                .sum();
    }

    @Override
    public long getAmountOfValBiggerThanAvg(List<Integer> integerList) {
        OptionalDouble averageOpt = integerList.stream().mapToDouble(i -> i).average();
        double avg = averageOpt.getAsDouble();
        return integerList.stream()
                .filter(i -> i > avg)
                .count();
    }


    @Override
    public long getNumberOfUniqueWords(List<String> strings) {
        return getSortedListOfUniqueWords(strings).stream().count();
    }

    @Override
    public List<String> getSortedListOfUniqueWords(List<String> strings) {
        String[] s = makeStringArrayOfSimpleWords(strings);

        return Arrays.stream(s)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(i -> i.getValue() == 1)
                .map(e -> e.getKey())
                .sorted()
                .collect(Collectors.toList());
    }

    private String[] makeStringArrayOfSimpleWords(List<String> strings) {
        return strings.stream()
                .map(e -> e + " ")
                .reduce("", String::concat)
                .split(" ");
    }

    @Override
    public Map<String, Long> occurrenceOfEachWord(List<String> strings) {
        String[] words = makeStringArrayOfSimpleWords(strings);
        return Arrays.stream(words)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @Override
    public Map<Character, Long> occurrenceOfEachSymbolNoUpper(List<String> strings) {
        Stream<Character> characterStream = strings.stream()
                .reduce("", String::concat)
                .chars()
                .mapToObj(c -> (char) c);
        logger.debug("calculating occurrence");

        return characterStream.filter(e-> !Character.isUpperCase(e))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
