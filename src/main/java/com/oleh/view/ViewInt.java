package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void maxAndAverage();
    void testCommand();
    void printStatistic();
    void testApp();
}
