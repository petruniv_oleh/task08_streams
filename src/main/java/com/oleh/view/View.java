package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.inter.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class View implements ViewInt {


    private Controller controller;

    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Show max and average of 3 numbers");
        menuMap.put("2", "2. Test commands");
        menuMap.put("3", "3. Get statistic for two random int list");
        menuMap.put("4", "4. Test application");

        menuMapMethods.put("1", this::maxAndAverage);
        menuMapMethods.put("2", this::testCommand);
        menuMapMethods.put("3", this::printStatistic);
        menuMapMethods.put("4", this::testApp);

    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void maxAndAverage() {
        logger.info("waiting for input");
        System.out.println("Enter values");
        System.out.print("a = ");
        int a = scanner.nextInt();
        System.out.print("b = ");
        int b = scanner.nextInt();
        System.out.print("c = ");
        int c = scanner.nextInt();

        int maxOfThreeNumb = controller.getMaxOfThreeNumb(a, b, c);
        int averageOfThreeNumb = controller.getAverageOfThreeNumb(a, b, c);

        System.out.println("Maximum of " + a + ", " + b + ", " + c + " is: " + maxOfThreeNumb);
        System.out.print("Average of " + a + ", " + b + ", " + c + " is: " + averageOfThreeNumb);
    }

    @Override
    public void testCommand() {
        System.out.println("Enter command name with it value");
        String line = scanner.nextLine();

        Stream<String> stream = Stream.of(line);

        List<String> collect = stream.flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.toList());

        Command command = controller.getCommand(collect.get(0));
        command.execute(collect.get(1));

    }

    @Override
    public void printStatistic() {

        List<Integer> randomInts1 = controller.getList1();
        List<Integer> randomInts2 = controller.getList1();
        logger.info("Start making stat for lists");
        Map<String, String> allStatisticForRandInt1 = controller.getAllStatisticForRandInt(randomInts1);
        Map<String, String> allStatisticForRandInt2 = controller.getAllStatisticForRandInt(randomInts2);
        logger.info("Outputting the statistic for lists of int");
        System.out.println(allStatisticForRandInt1.toString());
        System.out.println(allStatisticForRandInt2.toString());
    }

    @Override
    public void testApp() {
        List<String> stringList = new ArrayList<>();
        Scanner lineScanner = new Scanner(System.in);
        logger.info("Entering the words");
        String line = null;
        System.out.println("Enter some lines of words");
        do {

            line = lineScanner.nextLine();
            if (line.isEmpty()) {
                break;
            } else {
                stringList.add(line);
            }
        } while (true);

        logger.info("The end of entering words");

        System.out.println("There is " + controller.getNumberOfUniqueWords(stringList) + " unique words");
        System.out.println("All of them: ");

        List<String> sortedListOfUniqueWords = controller.getSortedListOfUniqueWords(stringList);
        sortedListOfUniqueWords.stream()
                .forEach(System.out::println);
        System.out.println("Occurrence of words: ");
        System.out.println(controller.getOccurrenceOfEachWord(stringList));
        System.out.println("Occurrence of symbols: ");
        System.out.println(controller.getSymbolsOccurrence(stringList));
    }
}
