package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.inter.Command;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Controller implements ControllerInt {

    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public Command getCommand(String s) {
        return model.testCommandPattern(s);
    }

    @Override
    public int getAverageOfThreeNumb(int a, int b, int c) {
        return model.getAvgOfThreeNumb(a, b, c);
    }

    @Override
    public int getMaxOfThreeNumb(int a, int b, int c) {
        return model.getMaxOfThreeNumb(a, b, c);
    }


    @Override
    public List<Integer> getList1() {
        return model.getRandomIntList();
    }

    @Override
    public List<Integer> getList2() {
        return model.getRandomIntListAnotherWay();
    }

    @Override
    public Map<String, String> getAllStatisticForRandInt(List<Integer> integerList) {
        Map<String, String> statistic = new LinkedHashMap<>();

        statistic.put("avg", "" + model.getAvg(integerList));
        statistic.put("max", "" + model.getMax(integerList));
        statistic.put("min", "" + model.getMin(integerList));
        statistic.put("sumWithSum", "" + model.countSumWithSum(integerList));
        statistic.put("sumWithReduce", "" + model.countSumWithReduce(integerList));
        statistic.put("amountOfValBiggerThanAvg", "" + model.getAmountOfValBiggerThanAvg(integerList));

        return statistic;
    }


    @Override
    public long getNumberOfUniqueWords(List<String> strings) {
        return model.getNumberOfUniqueWords(strings);
    }

    @Override
    public List<String> getSortedListOfUniqueWords(List<String> strings) {
        return model.getSortedListOfUniqueWords(strings);
    }

    @Override
    public Map<String, Long> getOccurrenceOfEachWord(List<String> strings) {
        return model.occurrenceOfEachWord(strings);
    }

    @Override
    public Map<Character, Long> getSymbolsOccurrence(List<String> strings) {
        return model.occurrenceOfEachSymbolNoUpper(strings);
    }
}
