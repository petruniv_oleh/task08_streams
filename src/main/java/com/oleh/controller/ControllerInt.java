package com.oleh.controller;

import com.oleh.model.inter.Command;
import java.util.List;
import java.util.Map;

public interface ControllerInt {
    Command getCommand(String s);

    int getAverageOfThreeNumb(int a, int b, int c);

    int getMaxOfThreeNumb(int a, int b, int c);

    List<Integer> getList1();

    List<Integer> getList2();

    Map<String, String> getAllStatisticForRandInt(List<Integer> integerList);

    long getNumberOfUniqueWords(List<String> strings);

    List<String> getSortedListOfUniqueWords(List<String> strings);

    Map<String, Long> getOccurrenceOfEachWord(List<String> strings);

    Map<Character, Long> getSymbolsOccurrence(List<String> strings);
}
